<?php

/* @var $this yii\web\View */

use yii\widgets\LinkPager;

$this->title = 'My Yii Application';
?>
<?= LinkPager::widget([
    'pagination' => $provider->pagination,
]);
?>
<div class="inside">
<?php foreach ($posts as $post): ?>
    <div class="post">
        <div class="post-content">
        </div>
            <div class="img">
                <img src="<?= $post->img ?>">
            </div>
            <div class="title">
                <?= $post->title ?>
            </div>
            <div class="text">
                <?= $post->text ?>
            </div>
            <div class="date">
            </div>
        </div>
<?php endforeach; ?>
</div>

