<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $img
 */
class Posts extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['title', 'text'], 'string'],
            [['img'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @return bool
     */
    public function upload()
    {

      /* echo Yii::$app->basePath;
        die;*/

        $date = date('U');
        $this->imageFile->saveAs(Yii::$app->basePath . "/../" . Yii::getAlias('frontend') . "/web/uploads/" . $this->imageFile->baseName . "_$date." . $this->imageFile->extension);
        $this->img = "/uploads/" . $this->imageFile->baseName . "_$date." . $this->imageFile->extension;

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'img' => 'Img',
        ];
    }
}
